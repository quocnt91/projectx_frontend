'use strict';

/**
 * @ngdoc function
 * @name projectxApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the projectxApp
 */
angular.module('projectxApp')
  .controller('HeaderCtrl', ['$scope', '$state', function($scope, $state) {
    $scope.$state = $state;
  }]);
