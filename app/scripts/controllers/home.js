'use strict';

/**
 * @ngdoc function
 * @name projectxApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the projectxApp
 */
angular.module('projectxApp')
  .controller('HomeCtrl', ['$scope', function($scope) {
    $scope.location = ["San Francisco", "New York", "Dallas", "Atlanta", "Chicago", "Los Angeles", "Las Vegas", "Philadelphia", "Baltimore", "Charlotte", "Columbia", "Savannah", "Denver", "Houston", "San Diego", "Seattle", "Austin", "San Antonio", "Miami", "Orlando", "Brooklyn"];
    $scope.people = [
      {name : "Gilbert Pickett", photo: "https://d220aniogakg8b.cloudfront.net/static/uploads/2015/02/11/91c07b76-9e1_2098316_183x183.jpg", stylis: "stylis"}
    ];

    $scope.search = function() {
    };

    function randomData() {
      for (var i=0 ; i < 4; i++) {
        $scope.people.push($scope.people[0]);
      }
    }

    randomData();
  }]);
