'use strict';

angular.module('projectxApp')
  .controller('ModalImageCtrl', ['$scope', '$modalInstance','user', 'images', 'index', function($scope, $modalInstance, user, images, index) {
    $scope.images = images;
    $scope.user = user;
    $scope.image = $scope.images[index];
    $scope.currentIndex = index;

    $scope.close = function() {
      $modalInstance.dismiss('cancel');
    };

    // Listen event from nav button
    $scope.$on('left', function() {
      $scope.handleImage('left');
    });

    $scope.$on('right', function() {
      $scope.handleImage('right');
    });

    $scope.changeImage = function(nav) {
      $scope.handleImage(nav);
    };

    $scope.handleImage = function(event) {
      if (event === 'left') {
        if ($scope.currentIndex === $scope.images.length - 1) {
          $scope.currentIndex = 0;
        } else {
          $scope.currentIndex ++;
        }
      } else {
        if ($scope.currentIndex === 0) {
          $scope.currentIndex = $scope.images.length - 1;
        } else {
          $scope.currentIndex --;
        }
      }
      $scope.image = $scope.images[$scope.currentIndex];
      $scope.$emit('changeImage', $scope.image);
    };



  }])
  .controller('ModalMessageMeCtrl', ['$scope', '$modalInstance' , function($scope, $modalInstance) {
    // Come here;

    $scope.close = function() {
    	$modalInstance.dismiss('cancel');
    };
  }])
  .controller('ModalApointmentCtrl', ['$scope', '$modalInstance', function($scope, $modalInstance) {

  	$scope.close = function() {
  		$modalInstance.dismiss('cancel');
  	}
  }])
  .controller('ModalWriteRecommendCtrl', ['$scope', '$modalInstance', 'user', function($scope, $modalInstance, user) {
  	$scope.user = user;
  	$scope.close = function() {
  		$modalInstance.dismiss('cancel');
  	}
  }]);
