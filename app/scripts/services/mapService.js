var projectApp = angular.module('projectxApp');

projectApp.factory('mapService',['$q', function($q) {
	return {
		getLoc : function(data) {
			var defer = $q.defer();
			if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
        	defer.resolve(position);
        });
	    } else {
	    	defer.resolve(null);
	    }
	    return defer.promise;
		}
	}
}]);